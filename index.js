const KBCleaner = require("./app");

//For running in a Lambda env
exports.handler = async(event, context) => {
    console.log(event);
    await KBCleaner.run();
    return context.logStreamName;
}

/**
 * This built-in environment variable is a dead giveaway that we're in an AWS environment
 */
if(!process.env.AWS_LAMBDA_FUNCTION_NAME) {
    KBCleaner.run();
}
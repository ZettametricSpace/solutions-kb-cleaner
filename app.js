const chalk = require("chalk");
const config = require("./config");

const ConfluenceService = require("./services/ConfluenceService");
const DatabaseService = require("./services/DatabaseService");
const ParserService = require("./services/ParserService");
const HTMLEntities = require("he");

const Timer = require("./services/TimerService");

const printTime = label => {
    let duration = Timer.stop();

    if(config.verbose) {
        console.log(`${label} took ${duration}s`);
    }
}

const getRunTime = duration => {
    let s = Math.floor(duration / 1000);
    let ms = duration % s;

    let m = Math.floor(s / 60);

    return `${m}m, ${s % 60}s, ${ms}ms`
};

exports.run = async() => {
    let startTime = +new Date();

    try {
        Timer.start();
        console.log(chalk.blue("Connecting to the database..."));
        await DatabaseService.init();
        printTime("Database connect");

        Timer.start();
        console.log(chalk.blue("Pulling articles from the database..."));
        let articles = await DatabaseService.getArticles();
        printTime("Pulling articles");

        Timer.start();
        console.log(chalk.blue("Pulling all pages from the Confluence API..."));
        let pages = await ConfluenceService.getAllPages();
        printTime("Confluence query");
        
        console.log(chalk.yellow(`Successfully fetched ${pages.length} pages.`));

        Timer.start();
        console.log(chalk.cyan("Looking for new articles..."));
        let newArticles = await ConfluenceService.findNewArticles(articles, pages);
        printTime("Confluence mapping");
        
        if(newArticles.length) {
            console.log(chalk.yellow(`Found ${newArticles.length} new articles.`));
            
            Timer.start();
            console.log(chalk.cyan("Fetching and mapping article data from Confluence..."));
            newArticles = await ConfluenceService.getDetailsForArticles(newArticles);
            articles.push(...newArticles);
            printTime("Confluence download");
        } else {
            console.log(chalk.yellow("No new articles found."));
        }
        
        Timer.start();

        if(config.forceUpdate) {
            articles = articles.map(x => ({
                ...x,
                title: HTMLEntities.decode(x.title)
            }));
        }

        console.log(chalk.cyan("Parsing articles and uploading attachments..."));
        let updates = await ParserService.convertRecords(articles);
        
        console.log(chalk.yellow(`Successfully processed ${updates.successes.length} articles, ${updates.failures.length} uploads failed!`));
        printTime("Article parsing");

        if(updates.failures.length) {
            if(!process.env.AWS_LAMBDA_FUNCTION_NAME) {
                require("fs").writeFileSync("debug.failures.json", JSON.stringify(updates.failures, null, 4));
            }

            console.log(chalk.red("Some attachments failed to upload. See debug JSON file for details"));
        }

        if(updates.successes.length) {
            Timer.start();
            console.log(chalk.blue("Upserting articles in the database..."));
            let { new: newRecords, updated } = await DatabaseService.upsertArticles(updates.successes, config.saveDatabaseChanges);

            console.log(chalk.yellow(`Successfully inserted ${newRecords} and updated ${updated} knowledgebase articles!`));
            printTime("Article upsert");
        } else {
            console.log(chalk.yellow("No updates are necessary, all images have already been parsed."));
        }
    } catch(ex) {
        console.error(ex);
    } finally {
        console.log(chalk.green("Closing database connection..."));
        DatabaseService.close();
        let runtime = (+new Date()) - startTime;

        if(config.verbose) {
            console.log(`Execution took ${getRunTime(runtime)}`);
        }
    }
};
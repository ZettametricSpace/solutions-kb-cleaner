# Solutions KnowledgeBase Cleaner

Attachments are not very fun with ServiceDesk/JIRA. They link to their internal CDN, which means if you try to embed it somewhere else, you better allow public access to that ServiceDesk, otherwise all embeds break.
This service will scour the database for Atlassian embeds, stream-upload them to S3, and automagically replace the image src to the new upload. This makes it so they can keep ServiceDesk private while still maintaining portal functionality. It's magic!

## Environment Variables (for AWS Lambda)
```
ECO_DB_USERNAME (database username)
ECO_DB_PASSWORD (database password)
ECO_DB_ADDRESS (database address)
ECO_DB_SCHEMA (database name)

ECO_AWS_BUCKET (bucket name, for example, uploaddemo.n-3-0.net)

ECO_CONFLUENCE_URL (JIRA URL)
ECO_CONFLUENCE_TOKEN (JIRA Token)
ECO_CONFLUENCE_SPACE_ID (space id, for example, EKB)

ECO_VERBOSE_OUTPUT (1|0)
ECO_SAVE_DATABASE_CHANGES (1|0)
```

## config.json (for normal execution)
```json
{
    "database": {
        "user": null,
        "password": null,
        "server": null,
        "database": null
    },
    "aws": {
        "accessKey": null,
        "secret": null,
        "bucket": null
    },
    "confluence": {
        "url": null,
        "token": null,
        "space": null
    },
    "verbose": false,
    "saveDatabaseChanges": true
}
```
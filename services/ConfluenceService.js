const config = require("../config");
const axios = require("axios");
const HTMLEntities = require("he");

const endpoint = new URL(`/wiki/rest/api`, config.confluence.url);
const UPDATE_INTERVAL = 100;

class ConfluenceService {
    get config() {
        return {
            headers: {
                "Authorization": `Basic ${config.confluence.token}`,
                "Accept": "application/json",
                "Content-Type": "application/json",
                "X-Atlassian-Token": "nocheck",
                "X-ExperimentalApi": "opt-in"
            },
            json: true,
        }
    }

    async getAllPages() {
        const dest = `${endpoint.toString()}/search?cql=(space=${config.confluence.space} and type=page)&limit=100000`;
        let res = await axios.get(dest, this.config);

        if(res.status === 200) {
            return res.data.results.filter(x => x.title.includes(x.content.id));
        } else throw new Error(`Confluence response: ${res.statusText}`);
    }

    async getPageDetails(id) {
        const dest = `${endpoint.toString()}/content/${id}?spaceKey=${config.confluence.space}&expand=space,body.view,version,container,metadata.labels`;
        let res = await axios.get(dest, this.config);

        if(res.status === 200) {
            return res.data;
        } else throw new Error(`Confluence response: ${res.statusText}`);
    }

    async findNewArticles(articles, potentials) {
        return potentials.filter(x => articles.findIndex(y => y.kbKey == x.content.id) < 0);
    }

    downloadFile(url) {
        return axios.get(url, { ...this.config, responseType: "stream" });
    }

    async getDetailsForArticles(articles) {
        let results = [];

        let count = 0;
        for(let potential of articles) {
            let details = await this.getPageDetails(potential.content.id);
            
            results.push({
                kbKey: potential.content.id,
                title: HTMLEntities.decode(potential.title),
                createDate: details.version.when,
                tags: details.metadata.labels.results.map(x => x.label).join(","),
                kbHTMLText: details.body.view.value,
                isNew: true
            });

            if(++count % UPDATE_INTERVAL === 0) {
                console.log(`Processed ${count}/${articles.length} records...`);
            }
        }

        return results;
    }
}

module.exports = new ConfluenceService();
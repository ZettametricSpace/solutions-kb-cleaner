const mssql = require("mssql");
const config = require("../config");
const uniqBy = require("lodash/uniqBy");

class DatabaseService {
    async init() {
        this.db = await mssql.connect(config.database);
    }

    close() {
        return this.db.close();
    }

    async getArticles() {
        const { db } = this;
        let results = await db.request().query(`select id, kbKey, kbTitle as title, kbHTMLText from [dbo].[knowledgeBase]`);
        return results.recordset;
    }

    async upsertArticles(articles, save = true) {
        let { db } = this;
        let transaction = new mssql.Transaction(this.db);

        try {
            transaction.begin();

            let result = { new: 0, updated: 0 };

            let modifiedArticles = uniqBy(articles, "kbKey");
            for(let { isNew, id, kbKey, title, createDate, tags, html } of modifiedArticles) {
                if(isNew) {
                    await db.request(transaction)
                        .input("key", kbKey)
                        .input("title", title)
                        .input("html", html)
                        .input("tags", tags)
                        .input("createDate", createDate)
                        .query(`insert into [dbo].[knowledgeBase](kbKey, kbTitle, distributionTypeId, kbHTMLText, insertedBy, updatedDTM, updatedBy, tags)
                                values(@key, @title, 2, @html, 1, @createDate, 1, @tags)`);
                    result.new++;
                } else if(config.forceUpdate) {
                    await db.request(transaction)
                        .input("id", id)
                        .input("html", html)
                        .input("title", title)
                        .query(`update [dbo].[knowledgeBase] set kbHTMLText = @html, kbTitle = @title where kbKey = @id`);
                    result.updated++;
                } else {
                    await db.request(transaction)
                        .input("id", id)
                        .input("html", html)
                        .query(`update [dbo].[knowledgeBase] set kbHTMLText = @html where kbKey = @id`);
                    result.updated++;
                }
            }

            if(save) {
                await transaction.commit();
            } else {
                await transaction.rollback();
            }

            return result;
        } catch(ex) {
            await transaction.rollback();
            throw ex;
        }
    }
}

module.exports = new DatabaseService();
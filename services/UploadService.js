const stream = require("stream");
const config = require("../config");
const AWS = require("aws-sdk");

//AWS will allow us through if the security is setup properly, so only setup auth if we're not in a Lambda env
if(!process.env.AWS_LAMBDA_FUNCTION_NAME) {
    AWS.config.accessKeyId = config.aws.accessKey;
    AWS.config.secretAccessKey = config.aws.secret;
}

const s3 = new AWS.S3();

exports.pipeUpload = name => res => new Promise((resolve, reject) => {
    let pt = new stream.PassThrough();

    s3.upload({
        Bucket: config.aws.bucket,
        Key: name,
        Body: pt
    }, (err, data) => {
        if(err) return reject(err);
        return resolve(data.Location);
    });

    res.data.pipe(pt);
});
const ConfluenceService = require("./ConfluenceService");
const cheerio = require("cheerio");
const config = require("../config");
const nanoid = require("nanoid");
const chalk = require("chalk");
const path = require("path");
const url = require("url");

const UploadService = require("./UploadService");

const uploadFile = (url, name) => new Promise((resolve, reject) => {
    ConfluenceService.downloadFile(url)
        .then(UploadService.pipeUpload(name))
        .then(name => resolve(name)) //URL to upload in S3
        .catch(ex => reject(ex));
});

const UPDATE_INTERVAL = 100;

exports.convertRecords = async(articles) => {
    let successes = [];
    let failures = [];

    let count = 0;

    const incrementCounter = () => {
        if(++count % UPDATE_INTERVAL === 0) {
            console.log(`Processed attachments for ${count} articles...`);
        }
    };

    const upload = (id, src, uid, extension, $) =>
        uploadFile(src, uid + extension)
            .then(res => $(`img[data-eco-uid="${uid}"]`)
                .attr("src", res)
                .removeAttr("data-image-src")
                .removeAttr("data-base-url")
                .removeAttr("srcset")
                .removeAttr("data-linked-resource-default-alias")
                .removeAttr("data-eco-uid"))
            .catch(ex => failures.push({ id, url: src, error: ex.message || ex }));

    for(let article of articles) {
        let { kbKey: id, isNew, kbHTMLText } = article;

        let $ = cheerio.load(kbHTMLText);

        let images = $("img");

        if(!images.length) {
            if(isNew) {
                successes.push({
                     ...article, id, isNew: true, html: kbHTMLText
                });
            } else if(config.forceUpdate) {
                successes.push({
                    ...article, id, isNew: false, html: kbHTMLText
                });
            }

            incrementCounter();
            continue;
        }

        let uploads = [];
        for(let i = 0; i < images.length; i++) {
            let img = images[i];
            let uid = nanoid(32);
            let src = img.attribs["src"];
            let parsed = url.parse(src);

            img.attribs["data-eco-uid"] = uid;

            //It's possible that the file name had Atlassian in it, so we want to further validate it's an SD embed
            if(/atlassian\.net/gmi.test(parsed.href) || parsed.href.startsWith("/")) {
                uploads.push(upload(id, src, uid, path.extname(parsed.pathname), $));
            }
        }

        try {
            if(uploads.length) {
                let res = await Promise.all(uploads);
                
                if(config.verbose) {
                    console.log(chalk.grey(`Processed ${res.length} uploads`));
                }
                
                successes.push({ ...article, id, html: $.html() });
            } else if(config.forceUpdate) {
                successes.push({ ...article, id, html: kbHTMLText });
            }
        } catch(ex) {
            console.error(ex);
        }

        incrementCounter();
    }

    return { successes, failures };
}
let time = null;

module.exports = {
    start: () => {
        time = process.hrtime();
    },

    stop: () => {
        if(!time) throw new Error("Timer not started!");
        let duration = process.hrtime(time);
        time = null;
        return (duration[0] + (duration[1] / 1e9)).toFixed(3);
    }
}
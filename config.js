const merge = require("lodash/merge");
const cloneDeep = require("lodash/cloneDeep");

const DEFAULT_CONFIG = {
    database: {
        user: null,
        password: null,
        server: null,
        database: null
    },
    aws: {
        accessKey: null,
        secret: null,
        bucket: null
    },
    confluence: {
        url: null,
        token: null,
        space: null
    },
    verbose: false,
    saveDatabaseChanges: true,
    forceUpdate: false
};

let cfg = cloneDeep(DEFAULT_CONFIG);
if(process.env.AWS_LAMBDA_FUNCTION_NAME) {
    let {
        ECO_DB_USERNAME,
        ECO_DB_PASSWORD,
        ECO_DB_ADDRESS,
        ECO_DB_SCHEMA,

        ECO_AWS_BUCKET,

        ECO_CONFLUENCE_URL,
        ECO_CONFLUENCE_TOKEN,
        ECO_CONFLUENCE_SPACE_ID,

        ECO_VERBOSE_OUTPUT,
        ECO_SAVE_DATABASE_CHANGES,
        ECO_FORCE_UPDATE_RECORDS
    } = process.env;

    merge(cfg, {
        database: {
            user: ECO_DB_USERNAME,
            password: ECO_DB_PASSWORD,
            server: ECO_DB_ADDRESS,
            database: ECO_DB_SCHEMA
        },
        aws: {
            bucket: ECO_AWS_BUCKET
        },
        confluence: {
            url: ECO_CONFLUENCE_URL,
            token: ECO_CONFLUENCE_TOKEN,
            space: ECO_CONFLUENCE_SPACE_ID
        },
        verbose: Boolean(ECO_VERBOSE_OUTPUT),
        saveDatabaseChanges: Boolean(ECO_SAVE_DATABASE_CHANGES),
        forceUpdate: Boolean(ECO_FORCE_UPDATE_RECORDS)
    });
} else {
    let config = require("./config.json");
    let env = process.env.NODE_ENV || "development";

    console.log(`Using a ${env} environment`)
    merge(cfg, config.environments[env] || config.environments["development"]);
}

module.exports = cfg;